// function createState(name, abbreviation) {
//     return `
//       <div class="card" style=width: 18rem;">
//         <img src="${pictureUrl}" class="card-img-top">
//         <div class="card-body">
//           <h5 class="card-title">${name}</h5>
//           <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
//           <p class="card-text">${description}</p>
//         </div>
//         <div class="card-footer">
//             <p class="card-text">${starts} - ${ends}</p>
//             </div>
//       </div>
//     `
//   }







window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('state');
        let x = 0

        for (let state of data.states) {
            const option = document.createElement("option");
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);

      }
    }
});
