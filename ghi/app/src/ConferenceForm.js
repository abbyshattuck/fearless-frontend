// import React, {useEffect, useState} from 'react';

// function ConferenceForm () {
//     const [name, setName] = useState('');
//     const [starts, setStarts] = useState('');
//     const [ends, setEnds] = useState("");
//     const [description, setDescription] = useState('');
//     const [maxPresentations, setMaxPresentations] = useState("");
//     const [maxAttendees, setMaxAttendees] = useState("");

//     const handleNameChange = (event) => {
//         const value = event.target.value;
//         setName(value);
//     }

//     const handleStartsChange = (event) => {
//         const value = event.target.value;
//         setStarts(value);
//     }

//     const handleEndsChange = (event) => {
//         const value = event.target.value;
//         setEnds(value);
//     }

//     const handleDescriptionChange = (event) => {
//         const value = event.target.value;
//         setDescription(value);
//     }

//     const handleMaxPresentationsChange = (event) => {
//         const value = event.target.value;
//         setMaxPresentations(value);
//     }

//     const handleMaxAttendeesChange = (event) => {
//         const value = event.target.value;
//         setMaxAttendees(value);
//     }

//     const handleSubmit = async (event) => {
//         event.preventDefault();
//         const data = {};

//         data.name = name;
//         data.starts = starts;
//         data.ends = ends;
//         data.description = description;
//         data.max_presentations = maxPresentations;
//         data.max_attendees = maxAttendees;

//         console.log(data);

//         const conferenceUrl = 'http://localhost:8000/api/conferences/';
//         const fetchConfig = {
//           method: "post",
//           body: JSON.stringify(data),
//           headers: {
//             'Content-Type': 'application/json',
//         },
//     };

//     const response = await fetch(conferenceUrl, fetchConfig);
//     if (response.ok) {
//       const newConference = await response.json();
//       console.log(newConference);

//       setName('');
//       setStarts('');
//       setEnds('');
//       setDescription('');
//       setMaxPresentations;
//       setMaxAttendees

//     }
//   }





//     const fetchData = async () => {
//       const url = 'http://localhost:8000/api/locations/';

//       const response = await fetch(url);

//       if (response.ok) {
//         const data = await response.json();
//         setLocations(data.location)
//         console.log("data:::", data.location);
//             }
//           }
//     useEffect(() => {
//       fetchData();
//     }, []);

//     return(
//         <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Create a new Conference</h1>
//             <form onSubmit={handleSubmit} id="create-conference-form">
//               <div className="form-floating mb-3">
//                 <input value={name} onChange={handleNameChange} placeholder="Name" required
//                 type="text" name="name" id="name" className="form-control"/>
//                 <label htmlFor="name">Name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={starts} onChange={handleStartsChange} placeholder="starts" required type="date" name="starts" id="starts" className="form-control"/>
//                 <label htmlFor="starts">Starts</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={city} onChange={handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
//                 <label htmlFor="ends">Ends</label>
//               </div>
//               {/* <div className="mb-3">
//                 <select value={state} onChange={handleStateChange} required name="state" id="state" className="form-select">
//                   <option value="">Choose a state</option>
//                   {states.map(state => {
//                     return (
//                         <option key={state.abbreviation} value={state.abbreviation}>
//                             {state.name}
//                             </option>
//                     );
//                   })}
//                 </select>
//               </div> */}
//               <button className="btn btn-primary">Create</button>
//             </form>
//           </div>
//         </div>
//       </div>
//     );

//   }
// export default ConferenceForm;
