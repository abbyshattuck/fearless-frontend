import React from 'react';


function Nav() {
    return (
<div className="container">
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <a className="navbar-brand" href="#">Conference GO!</a>

      <li>
        <a href="#">Home </a>
      </li>
      <li>
        <a href="#">New Location</a>
      </li>
      <li>
        <a href="#">New Conference</a>
      </li>
      <li>
        <a href="#">New Presentation</a>
      </li>
  </nav>
</div>


    );
}

export default Nav;
